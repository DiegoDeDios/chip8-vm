#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "chip8_vm.h"

struct Chip8{
    uint_8 registers[REGISTERS]; //V0....VF
    uint_8  memory[MEMORY_SIZE]; //4Kb of memory
    uint_8 display[DISPLAY_W * DISPLAY_H];
    bool draw_flag : 1;
    uint_16 PC; //Program Counter
    uint_16 I; //Index register
    uint_16 stack[STACK_SIZE]; //Stack
    uint_16 SP; //Stack pointer
    uint_8 delay_t;
    uint_8 sound_t;

}chip8;

//Opcodes
//0x0NNN
void call(struct Chip8 *chip8){
    printf("Pass");
    chip8->PC+=2;
}

//0x00E0
void clear_display(struct Chip8 *chip8){
    memset(chip8->display, 0x0, sizeof(uint_8) * (DISPLAY_W*DISPLAY_H));
    chip8->draw_flag = True;
    }

//0x00EE
void ret(struct Chip8 *chip8){ //Return from subroutine
    chip8->PC = chip8->stack[--chip8->SP];

}
//0x1NNN
void go_to(struct Chip8 *chip8, uint_16 nnn){
    chip8->PC = nnn;
}
//0x2NNN
void call_sbr(struct Chip8 *chip8,uint_16 nnn){
    chip8->stack[chip8->SP++] = chip8->PC+2; //Pushing PC into stack to remember where to jump back after subroutine ends
    chip8->PC = nnn;
}
//0x3XNN
void jp_eq(struct Chip8 *chip8,uint_8 x, uint_8 nn){
    if (chip8->registers[x] == nn){
        chip8->PC += 4; 
    }
    else{
        chip8->PC += 2; 
    }
}

//0x4XNN
void jp_neq(struct Chip8 *chip8,uint_8 x, uint_8 nn){
    if (chip8->registers[x] != nn){
        chip8->PC += 4; 
    }
    else{
        chip8->PC += 2; 
    }
}

//0x5XY0
void jp_req(struct Chip8 *chip8,uint_8 x,uint_8 y){
    if(chip8->registers[x]== chip8->registers[y]){
        chip8->PC += 4;
    }
    else{
        chip8->PC += 2;
    }
}

//0x6XNN
void set_reg(struct Chip8 *chip8,uint_8 x,uint_8 nn){
    chip8->registers[x] = nn;
    chip8->PC += 2;
}

//0x7XNN
void add_reg(struct Chip8 *chip8,uint_8 x,uint_8 nn){
    chip8->registers[x] +=nn;
    chip8->PC += 2;
}

//0x8XY0
void set_reg2reg(struct Chip8 *chip8,uint_8 x, uint_8 y){
    chip8->registers[x] = chip8->registers[y];
    chip8->PC += 2;
}

//0x8XY1 OR
void or_reg(struct Chip8 *chip8,uint_8 x, uint_8 y){
    chip8->registers[x] |= chip8->registers[y];
    chip8->PC += 2;
}

//0x8XY2
void and_reg(struct Chip8 *chip8,uint_8 x, uint_8 y){
    chip8->registers[x] &= chip8->registers[y];
    chip8->PC += 2;
}

//0x8XY3
void xor_reg(struct Chip8 *chip8,uint_8 x, uint_8 y){
    chip8->registers[x] ^= chip8->registers[y];
    chip8->PC += 2;
}

//0x8XY4
void add_reg2reg(struct Chip8 *chip8,uint_8 x, uint_8 y){
    chip8->registers[x] += chip8->registers[y];
    if((uint_16)chip8->registers[x] + (uint_16)chip8->registers[y] > 0xff){ //Dear lord forgive me
        chip8->registers[0xf] = 1;
    }
    else{
        chip8->registers[0xf] = 0;
    }
    chip8->PC += 2; 
}


//0x8XY5
void sub_regy2regx(struct Chip8 *chip8,uint_8 x, uint_8 y){
    chip8->registers[x] -= chip8->registers[y];
    if(chip8->registers[x] > chip8->registers[y]){ //If there's a wraparound then x will be greater than y
        chip8->registers[0xf] = 1;
    }
    else{
        chip8->registers[0xf] = 0;
    }
    chip8->PC += 2; 
}


//0x8XY6

void shift_reg_right(struct Chip8 *chip8,uint_8 x){
    chip8->registers[0xF] = (chip8->registers[x] & 0b00000001); //Store in VF LSB
    chip8->registers[x] = chip8->registers[x] >> 1;
    chip8->PC += 2; 
}

//0x8XY7
void sub_regx2regy(struct Chip8 *chip8,uint_8 x, uint_8 y){
    chip8->registers[y] -= chip8->registers[x];
    if(chip8->registers[y] > chip8->registers[x]){ 
        chip8->registers[0xf] = 1;
    }
    else{
        chip8->registers[0xf] = 0;
    }
    chip8->PC += 2; 
}


//0x8XYE
void shift_reg_left(struct Chip8 *chip8,uint_8 x){
    chip8->registers[0xF] = (chip8->registers[x] & 0b10000000); //Store in VF LSB
    chip8->registers[x] = chip8->registers[x] << 1;
    chip8->PC += 2; 
}

//0x9XY0
void jp_rneq(struct Chip8 *chip8,uint_8 x,uint_8 y){
    if(chip8->registers[x] != chip8->registers[y]){
        chip8->PC += 4;
    }
    else{
        chip8->PC += 2;
    }
}

//0xANNN
void set_index(struct Chip8 *chip8, uint_16 nnn){
    chip8->I = nnn;
    chip8->PC +=2;
}

//0xBNNN
void jmp_addr(struct Chip8 *chip8, uint_16 nnn){
    chip8->PC =chip8->registers[0] + nnn;
}

//0xCXNN 
void set_reg2rand(struct Chip8 *chip8, uint_8 x, uint_8 nn){
    // Pending: Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
    chip8->PC +=2;
}

//0xDXYN
void draw(struct Chip8 *chip8, uint_8 x, uint_8 y, uint_8 n){
    uint_16 pixel;
    chip8->registers[0xF] = 0x0; //Setting the draw flag to false
    for(uint_16 col = 0; col < n; col++){
        pixel = chip8->memory[chip8->I + col]; //Which pixel on the sprite is on index address
        for(uint_16 row = 0; row < 8; row++){
            uint_16 pixel_coord = x + row + ((y + col) * 64);
            if((pixel & (0x80 >> row)) != 0){ //If the pixel on the sprite is not 0... check for collision and draw
                chip8->registers[0xF] = chip8->display[pixel_coord] == 1? 1:0; //This is the collision check
                chip8->display[pixel_coord] ^= 1; //We turn on/off the pixl
            }
        }
    }
    chip8->draw_flag = True;
    chip8->PC+=2;

}

//0xEX9E

void is_key_pressed(struct Chip8 *chip8, uint_8 x){
//Remind me to change for x ? y : z syntax eventually 
    if(keypad[chip8->registers[x]]){
        chip8->PC+=4;
    }
    else{
        chip8->PC+=2;
    }
}

//0xEXA1
void is_key_not_pressed(struct Chip8 *chip8, uint_8 x){
//Remind me to change for x ? y : z syntax eventually 
    if(!keypad[chip8->registers[x]]){
        chip8->PC+=4;
    }
    else{
        chip8->PC+=2;
    }
}

// 0xFX07	

void get_delay_timer(struct Chip8 *chip8, uint_8 x){
    chip8->registers[x] = chip8->delay_t;
    chip8->PC+=2;
}

// 0xFX0A	
void get_key(struct Chip8 *chip8, uint_8 x){
    bool key_flag = False;
    while (!key_flag) {
        for (uint_16 i = 0; i < KEYBOARD_SIZE; i++) {
            if (keypad[i]) {
                chip8->registers[x] = i;
                key_flag = True;
            }
        }
    }
    chip8->PC += 2;
}


// 0xFX15	
void set_delay_timer(struct Chip8 *chip8, uint_8 x){
    chip8->delay_t += chip8->registers[x];
    chip8->PC+=2;
}

// 0xFX18	

void set_sound_timer(struct Chip8 *chip8, uint_8 x){
    chip8->sound_t += chip8->registers[x];
    chip8->PC+=2;  
}

// 0xFX1E	

void add_to_index(struct Chip8 *chip8, uint_8 x){
    chip8->I += chip8->registers[x];
    chip8->PC+=2;
}

// 0xFX29	
void set_sprite_address(struct Chip8 *chip8, uint_8 x){
    chip8->I = FONTSET_BYTES * chip8->registers[x];
    chip8->PC+=2;
}

// 0xFX33	
void store_bcd(struct Chip8 *chip8, uint_8 x){
    chip8->memory[chip8->I] = (uint_8)(chip8->registers[x]%1000)/100; //Hudreds
    chip8->memory[chip8->I+1] = (uint_8)(chip8->registers[x]%100)/10; //Tens
    chip8->memory[chip8->I+2] = (uint_8)(chip8->registers[x]%10);// Ones
    chip8->PC+=2;
}
// 0xFX55	
void dump_to_mem(struct Chip8 *chip8, uint_8 x){
    for(uint_16 i =0; i <= x; i++){
        chip8->memory[chip8->I + i] = chip8->registers[i];
    }
    chip8->PC+=2;
}

// 0xFX65
void load_from_mem(struct Chip8 *chip8, uint_8 x){
    for(uint_16 i =0; i <= x; i++){
        chip8->registers[i] = chip8->memory[chip8->I + i];
    }
    chip8->PC+=2;
}

//Aux Functions

void dump_registers(struct Chip8 *chip8){
    for(int i = 0; i < REGISTERS; i++){
        printf("V%x: 0x%x\n",i,chip8->registers[i]);
    }
    printf("SP: 0x%x\n",chip8->SP);
    printf("I: 0x%x\n",chip8->I);
    printf("PC: 0x%x\n",chip8->PC);
}

void dump_memory(struct Chip8 *chip8){
    for(uint_16 i = 0x0; i < MEMORY_SIZE; i++){
        if(i % 0x100 == 0){
            printf("\n%x:",i);
        }
        printf("0x%x ",chip8->memory[i]);
    }
    printf("\n");
}

void load_into_memory(struct Chip8 *chip8, char *rom_path){
    size_t fd = open(rom_path, O_RDONLY);
    size_t start_offset = lseek(fd, 0, SEEK_CUR);
    size_t size = lseek(fd, (size_t)0, SEEK_END); 
    printf("Size: %zu\n",size);
    lseek(fd, start_offset, SEEK_SET);
    read(fd, &chip8->memory[0x200],size);
}

void initialize_chip8(struct Chip8 *chip8){
    chip8->PC = 0x200; //Start address of program
    chip8->I = 0x0;
    chip8->SP = 0x0;
    chip8->draw_flag = True;
    //Fill registers with 0x0
    memset(chip8->registers, 0x0, sizeof(uint_8)*REGISTERS); 
    //Fill memory with 0x0
    memset(chip8->memory, 0x0, sizeof(uint_8)*MEMORY_SIZE);
    //Clear Stack
    memset(chip8->stack,0x0, sizeof(uint_16)*STACK_SIZE);
    //Clear Display
    memset(chip8->display, 0x0, sizeof(uint_8) * (DISPLAY_W*DISPLAY_H));
    //Clear inputs
    memset(keypad, 0x0, sizeof(uint_8)*KEYBOARD_SIZE);

    //Lets load the fontset to memory
    for(uint_16 i = 0; i < 80; i++){
        /*For reference: http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#dispcoords
        "These sprites are 5 bytes long, or 8x5 pixels. The data should be stored in the interpreter area of Chip-8 memory (0x000 to 0x1FF)."*/
        chip8->memory[0x0+i] = sprite_table[i]; //Loading into memory fontset sprites starting at address 0x0
    }
}

uint_16 fetch(struct Chip8 *chip8){
    uint_16 opcode = (chip8->memory[chip8->PC] << 8)|(chip8->memory[chip8->PC+1]); //Chip8 is big endian
    return opcode;
}

void decode_execute_opcode(struct Chip8 *chip8){
    //Decode instruction
    uint_16 instruction = fetch(chip8);
    //Since all opcodes use X,Y we might aswell declare it here...
    uint_8 x,y,nn,n;
    uint_16 nnn;
    x = (instruction&0x0f00)>>8;
    y = (instruction&0x00f0)>>4;
    nnn = instruction & 0x0fff;
    nn = instruction & 0x00ff;
    n = instruction & 0x000f;
    printf("The instruction is: 0x%x\n",instruction);
    switch(instruction >> 12){ //MSB nibble
        case(0x0):
            switch (nnn)
            {
            case(0x0e0):
                clear_display(chip8);
                break;
            case(0x0ee):
                ret(chip8);
                break;
            default:
                printf("Pass");
                break;
            }
            break;
        case(0x1):
            go_to(chip8,nnn);
            break;
        case(0x2):
            call_sbr(chip8,nnn);
            break;
        case(0x3):
            jp_eq(chip8, x, nn);
            break;
        case(0x4):
            jp_neq(chip8, x, nn);
            break;
        case(0x5):
            jp_req(chip8,x,y);
            break;
        case(0x6):
            set_reg(chip8,x, nn);
            break;
        case(0x7):
            add_reg(chip8,x, nn);
            break;
        case(0x8):
            switch (n)
            {
            case(0x0):
                set_reg2reg(chip8,x,y);
                break;
            case(0x1):
                or_reg(chip8,x,y);
                break;
            case(0x2):
                and_reg(chip8,x,y);
                break;
            case(0x3):
                xor_reg(chip8,x,y);
                break;
            case(0x4):
                add_reg2reg(chip8,x,y);
                break;
            case(0x5):
                sub_regy2regx(chip8,x,y);
                break;
            case(0x6):
                shift_reg_right(chip8,x);
                break;
            case(0x7):
                sub_regx2regy(chip8,x,y);
                break;
            case(0xe):
                shift_reg_left(chip8,x);
                break;
            default:
                printf("Invalid opcode of 0x8III form\n");
                break;
            } 
            break;
        case(0x9):
            jp_rneq(chip8,x,y);
            break;
        case(0xa):
            set_index(chip8,nnn);
            break;
        case(0xb):
            jmp_addr(chip8,nnn);
            break;
        case(0xc):
            set_reg2rand(chip8,x,nn);
            break;
        case(0xd):
            draw(chip8,x,y,n);
            break;
        case(0xe):
            switch(nn){
                case(0x9e):
                    break;
                case(0xa1):
                    break;
                default:
                    printf("Unknown opcode of the form 0xEXII\n");
            }
            break;
        case(0xf):
            switch (nn)
            {
            case(0x07):
                get_delay_timer(chip8,x);
                break;
            case(0x0a):
                get_key(chip8,x);
                break;
            case(0x15):
                set_delay_timer(chip8,x);
                break;
            case(0x18):
                set_sound_timer(chip8,x);
                break;
            case(0x1e):
                add_to_index(chip8,x);
                break;
            case(0x29):
                set_sprite_address(chip8,x);
                break;
            case(0x33):
                store_bcd(chip8,x);
                break;
            case(0x55):
                dump_to_mem(chip8,x);
                break;
            case(0x65):
                load_from_mem(chip8,x);
                break;
            
            default:
                printf("Unknown opcode of the form 0xFXII\n");
                break;
            }
            break;
        default:
            printf("Opcode unkown\n");
            break;
    }

}

void emulate_clk_cycle(struct Chip8 *chip8){
    decode_execute_opcode(chip8);
    sleep(1);
}

int main(int argc, char **argv){
    struct Chip8 *chip8;
    chip8 = (struct Chip8*)malloc(sizeof(struct Chip8));
    initialize_chip8(chip8);
    load_into_memory(chip8,argv[1]);
    //dump_memory(chip8);

    int x = 0;
    while(x<atoi(argv[2])){//Emulate first x instructions
        emulate_clk_cycle(chip8);
        x++;
    }
    return 0;
}