/* CHIP8 Spec
Src: http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#4.0
4KB of RAM Memory (4096 bytes)
Big Endian
16 General purpose registers (1 byte each): V0....VF
A 16 bit Index (I) register
The stack is an array of 16 16-bit values
A 8 bit Stack Pointer (SP) register for calls and jumps
The Program Counter (PC) 16 bit
The Keyboard was a 16 keys keypad (0x0...0xF) 
A 64 x 32 GFX display 

Memory Map:
+---------------+= 0xFFF (4095) End of Chip-8 RAM
|               |
|               |
|               |
|               |
|               |
| 0x200 to 0xFFF|
|     Chip-8    |
| Program / Data|
|     Space     |
|               |
|               |
|               |
+- - - - - - - -+= 0x600 (1536) Start of ETI 660 Chip-8 programs
|               |
|               |
|               |
+---------------+= 0x200 (512) Start of most Chip-8 programs
| 0x000 to 0x1FF|
| Reserved for  |
|  interpreter  |
+---------------+= 0x000 (0) Start of Chip-8 RAM
*/

#define REGISTERS 0x10
#define STACK_SIZE 0xF
#define KEYBOARD_SIZE 0x10
#define SPRITE_TABLE_SIZE 0x50
#define DISPLAY_W 64
#define DISPLAY_H 32
#define MEMORY_SIZE 0x1000
#define FONTSET_BYTES 0x5
#define uint_8 unsigned char
#define uint_16 unsigned short
#define bool unsigned char
#define True 0x1
#define False 0x0
struct Chip8;
//Sprite table located at address 0x0
unsigned short sprite_table[SPRITE_TABLE_SIZE] = 
{
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F 
};

//Keypad layout
unsigned char keypad[KEYBOARD_SIZE];

uint_16 fetch(struct Chip8 *chip8);
void initialize_chip8(struct Chip8 *chip8);
void load_into_memory(struct Chip8 *chip8,char *rom_path);
void decode_execute_opcode(struct Chip8 *chip8);
void emulate_cycle(struct Chip8 *chip8); //Fetch,Decode,Execute cycle
void dump_registers(struct Chip8 *chip8);
void dump_memory(struct Chip8 *chip8);


