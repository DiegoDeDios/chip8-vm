# Compiling with GCC

`gcc -o chip8.out chip8_vm.c`

# Running

`./chip8.out /path/to/rom <instructions_to_execute>`
